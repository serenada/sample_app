require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
  end
  
  
  test "layout links unlogged" do
    get root_path
    assert_template 'static_pages/home'
    #header
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", login_path
    #middle
    assert_select "a[href=?]", signup_path
    #footer
    assert_select "a[href=?]", about_path, text:"About"
    assert_select "a[href=?]", contact_path
    get login_path
    assert_select "title", full_title("Log in")     
    get help_path
    assert_select "title", full_title("Help")    
    get contact_path
    assert_select "title", full_title("Contact")
    get about_path
    assert_select "title", full_title("About")
    get signup_path
    assert_select "title", full_title("Sign up")
  end
  
  
  test "layout links logged" do
    log_in_as(@user)
    get root_path
    assert_template 'static_pages/home'
    #header
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", users_path
    assert_select "a[href=?]", user_path(@user)
    assert_select "a[href=?]", edit_user_path(@user)
    assert_select "a[href=?]", logout_path
    #middle
    assert_select "a[href=?]", signup_path
    #footer
    assert_select "a[href=?]", about_path, text:"About"
    assert_select "a[href=?]", contact_path
    get login_path
    assert_select "title", full_title("Log in")     
    get help_path
    assert_select "title", full_title("Help")    
    get contact_path
    assert_select "title", full_title("Contact")
    get about_path
    assert_select "title", full_title("About")
    get signup_path
    assert_select "title", full_title("Sign up")
  end  
  
end
