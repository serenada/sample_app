require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  test "invalid signup information" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name:  "",
                                         email: "user@invalid",
                                         password:              "foo",
                                         password_confirmation: "bar" } }
    end
    assert_template 'users/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
    assert_select 'input', :id=>'user_name'
    assert_select 'label', :for=>'user_name'
    assert_select 'input', :id=>'user_email'
    assert_select 'label', :for=>'user_email'
    assert_select 'input', :id=>'user_password'
    assert_select 'label', :for=>'user_password'
    assert_select 'input', :id=>'user_password_confirmation'
    assert_select 'label', :for=>'user_password_confirmation'    
    assert_select 'form', {:action=>'/users', :method=>'post'}
    assert_select 'form[action="/signup"]'

  end
  
  test "valid signup information" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, params: { user: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    end
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
    assert_not flash.empty?
    assert_not flash[:success].empty?
  end  
  
end
